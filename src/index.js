var app = new Vue({
    el: "#app",
    data: {
        output: ""
    }
});

/**
 * 
 * @param {string} value The input value
 */
function onInputChanged(value){
    
    app.output = pigify(value);
}

/**
 * 
 * @param {string} s 
 * @returns {string}
 */
function pigify(s){
    if(s.length === 0){
        return "Keine Eingabe...";
    }
    let sArr = s.split(" ").filter((el) => {
        return el != "" && el != " ";
    });
    //Alle wörter separat

    for(let i = 0; i < sArr.length; i++){
        const word = sArr[i].split("");
        if(!isConsonant(word[0])){
            continue;
        }
        let firstChar = word[0];
     
        word.shift();
  
        if(firstChar === firstChar.toUpperCase()){
            //1st char is uppercase
            if(typeof word[0] === "string")
            word[0] = word[0].toUpperCase();
        }
 
        sArr[i] = word.join("") + firstChar.toLowerCase() + "ay";   
    }
    return sArr.join(" ");

    /**
     *
     *
     * @param {string} s
     * @returns {boolean}
     */
    function isConsonant(s){
 
        const vocals = "aeiouäöü".split("");
        for(const el of vocals){
            if(el === s.toLowerCase())
                return false;
        }
        return true;
    }
}